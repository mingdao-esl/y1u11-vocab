#+title: Music 
#+author: Unit 11 Vocabulary
#+EXPORT_FILE_NAME: index

:REVEAL_PROPERTIES:
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: serif
#+OPTIONS: toc:nil timestamp:nil num:nil
:end:

* Part 1 & 2
_Part 1_
#+ATTR_HTML: :border 2 :rules all :frame border
| *Mozart* | *Beethoven*     | *Bach*            |
| southern | western         | Michael Jackson   |
| king     | *Symphony Hall* | *style*           |
_Part 2_
#+ATTR_HTML: :border 2 :rules all :frame border
| accept | decline    |
| plans  | invitation |
** Mozart, Beethoven, Bach
[[file:assets/classical-composers.jpg]]
** Symphony Hall (/n/.)
[[file:assets/symphony-hall.jpg]]
** Style (/music, n/.)
A type of music
- [[https://youtu.be/jv2WJMVPQi8][classical]]
- [[https://youtu.be/gSJeHDlhYls][hip-hop]]
- [[https://youtu.be/sOnqjkJTMaA?t=279][pop (popular music)]]
- [[https://youtu.be/I_2D8Eo15wE][rock]]
- [[https://youtu.be/tT9Eh8wNMkw][jazz]]
- [[https://youtu.be/klSC9QzmGzo?t=16][country]]
- [[https://youtu.be/rf8GjhXvOjU][reggae]]
- [[https://youtu.be/kdemFfbS5H0?t=37][electronic]]

* Part 4
#+ATTR_HTML: :border 2 :rules all :frame border
| scan     | album      | *Grammy Award* |
| win      | *Lebanese* | *Latin*        |
| *Arabic* | success    | copies         |
| lessons  | continued  | hit            |
| theme    | global     | timeline       |
| laundry  | service    | *Colombia*     |
| mix      |            |                |

** Grammy Award (/n/.)
An award for the best music and musicians.

#+ATTR_HTML: :height 500px
[[file:assets/grammy-award.jpg]]
** Lebanese (/adj/.)
From Lebanon.

#+ATTR_HTML: :height 500px
[[file:assets/lebanon-map.jpg]]
** Latin (/adj./)
From Spanish or Portuguese-speaking countries.

#+ATTR_HTML: :height 500px
[[file:assets/latin-america.png]]
** Arabic (/adj./)
From an Arab country or culture.

#+ATTR_HTML: :height 500px
[[file:assets/arabian-penninsula.gif]]
* Part 5
#+ATTR_HTML: :border 2 :rules all :frame border
| holiday              | samba         | body language |
| interpreting         | *traditional* | national      |
| celebrate            | party         | station       |
| origin               | drum          | develop       |
| century              | *carnival*    | *parade*      |
| bossa nova           | costumes      | original      |
| residents (Cariocas) | advice        | bars          |
| setting              | *fusion*      |               |

** parade (/n./)
#+ATTR_HTML: :height 500px
[[file:assets/taiwan-national-day-parade.jpg]]
** costumes (/n./)
#+ATTR_HTML: :height 500px
[[file:assets/costume-carnival.jpg]]
** carnival (/n./)
#+REVEAL_HTML: <div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"> <iframe style="width:75%;height:75%;position:absolute;left:125px;top:0px;overflow:hidden" src="https://www.youtube.com/embed/AsNoHinDidU?si=saTcsgBqVnVusDN-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
** traditional (/adj./)
Something people have done for a long time.

#+ATTR_HTML: :height 500px
[[file:assets/traditional-instruments.jpg]]
** fusion (/n./)
Two or more things are combined into one thing.

#+ATTR_HTML: :height 500px
[[file:assets/fusion-meme-dbz.png]]
* Part 6
#+ATTR_HTML: :border 2 :rules all :frame border
| acoustics | atmosphere | blues          |
| important | punk       | tavern/bar/pub |
| horseshoe |            |                |
* Workbook Part 4
#+ATTR_HTML: :border 2 :rules all :frame border
| Luciano Pavarotti | opera               | Italy      |
| chorus            | salesman            | appear     |
| perform           | television programs | tenors     |
| recording         | copies              | raise      |
| money             | charity             | earthquake |
| pancreatic cancer | teenager            | throughout |
* Workbook Part 6
#+ATTR_HTML: :border 2 :rules all :frame border
| origin        | beat        | accordion            |
| rhythmic      | drums       | stringed instruments |
| Latin America | tambourines |                      |
